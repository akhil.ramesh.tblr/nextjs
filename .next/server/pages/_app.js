/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./components/Feature.js":
/*!*******************************!*\
  !*** ./components/Feature.js ***!
  \*******************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _storyblok_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @storyblok/react */ \"@storyblok/react\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_storyblok_react__WEBPACK_IMPORTED_MODULE_1__]);\n_storyblok_react__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\nconst Feature = ({ blok  })=>/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        className: \"column feature\",\n        ...(0,_storyblok_react__WEBPACK_IMPORTED_MODULE_1__.storyblokEditable)(blok),\n        children: blok.name\n    }, void 0, false, {\n        fileName: \"/home/toobler/Desktop/nextjs/components/Feature.js\",\n        lineNumber: 4,\n        columnNumber: 3\n    }, undefined);\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Feature);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jb21wb25lbnRzL0ZlYXR1cmUuanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBO0FBQXFEO0FBRXJELE1BQU1DLFVBQVUsQ0FBQyxFQUFFQyxLQUFJLEVBQUUsaUJBQ3ZCLDhEQUFDQztRQUFJQyxXQUFVO1FBQWtCLEdBQUdKLG1FQUFpQkEsQ0FBQ0UsS0FBSztrQkFDeERBLEtBQUtHLElBQUk7Ozs7OztBQUlkLGlFQUFlSixPQUFPQSxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vYmxvZy8uL2NvbXBvbmVudHMvRmVhdHVyZS5qcz8wZjMzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHN0b3J5Ymxva0VkaXRhYmxlIH0gZnJvbSBcIkBzdG9yeWJsb2svcmVhY3RcIjtcbiBcbmNvbnN0IEZlYXR1cmUgPSAoeyBibG9rIH0pID0+IChcbiAgPGRpdiBjbGFzc05hbWU9XCJjb2x1bW4gZmVhdHVyZVwiIHsuLi5zdG9yeWJsb2tFZGl0YWJsZShibG9rKX0+XG4gICAge2Jsb2submFtZX1cbiAgPC9kaXY+XG4pO1xuIFxuZXhwb3J0IGRlZmF1bHQgRmVhdHVyZTsiXSwibmFtZXMiOlsic3RvcnlibG9rRWRpdGFibGUiLCJGZWF0dXJlIiwiYmxvayIsImRpdiIsImNsYXNzTmFtZSIsIm5hbWUiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./components/Feature.js\n");

/***/ }),

/***/ "./components/Grid.js":
/*!****************************!*\
  !*** ./components/Grid.js ***!
  \****************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _storyblok_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @storyblok/react */ \"@storyblok/react\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_storyblok_react__WEBPACK_IMPORTED_MODULE_1__]);\n_storyblok_react__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\nconst Grid = ({ blok  })=>{\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        className: \"grid\",\n        ...(0,_storyblok_react__WEBPACK_IMPORTED_MODULE_1__.storyblokEditable)(blok),\n        children: blok.columns.map((nestedBlok)=>/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_storyblok_react__WEBPACK_IMPORTED_MODULE_1__.StoryblokComponent, {\n                blok: nestedBlok\n            }, nestedBlok._uid, false, {\n                fileName: \"/home/toobler/Desktop/nextjs/components/Grid.js\",\n                lineNumber: 7,\n                columnNumber: 9\n            }, undefined))\n    }, void 0, false, {\n        fileName: \"/home/toobler/Desktop/nextjs/components/Grid.js\",\n        lineNumber: 5,\n        columnNumber: 5\n    }, undefined);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Grid);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jb21wb25lbnRzL0dyaWQuanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBO0FBQXlFO0FBRXpFLE1BQU1FLE9BQU8sQ0FBQyxFQUFFQyxLQUFJLEVBQUUsR0FBSztJQUN6QixxQkFDRSw4REFBQ0M7UUFBSUMsV0FBVTtRQUFRLEdBQUdMLG1FQUFpQkEsQ0FBQ0csS0FBSztrQkFDOUNBLEtBQUtHLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDLENBQUNDLDJCQUNqQiw4REFBQ1AsZ0VBQWtCQTtnQkFBQ0UsTUFBTUs7ZUFBaUJBLFdBQVdDLElBQUk7Ozs7Ozs7Ozs7QUFJbEU7QUFFQSxpRUFBZVAsSUFBSUEsRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovL2Jsb2cvLi9jb21wb25lbnRzL0dyaWQuanM/OWI3YyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBzdG9yeWJsb2tFZGl0YWJsZSwgU3RvcnlibG9rQ29tcG9uZW50IH0gZnJvbSBcIkBzdG9yeWJsb2svcmVhY3RcIjtcbiBcbmNvbnN0IEdyaWQgPSAoeyBibG9rIH0pID0+IHtcbiAgcmV0dXJuIChcbiAgICA8ZGl2IGNsYXNzTmFtZT1cImdyaWRcIiB7Li4uc3RvcnlibG9rRWRpdGFibGUoYmxvayl9PlxuICAgICAge2Jsb2suY29sdW1ucy5tYXAoKG5lc3RlZEJsb2spID0+IChcbiAgICAgICAgPFN0b3J5Ymxva0NvbXBvbmVudCBibG9rPXtuZXN0ZWRCbG9rfSBrZXk9e25lc3RlZEJsb2suX3VpZH0gLz5cbiAgICAgICkpfVxuICAgIDwvZGl2PlxuICApO1xufTtcbiBcbmV4cG9ydCBkZWZhdWx0IEdyaWQ7Il0sIm5hbWVzIjpbInN0b3J5Ymxva0VkaXRhYmxlIiwiU3RvcnlibG9rQ29tcG9uZW50IiwiR3JpZCIsImJsb2siLCJkaXYiLCJjbGFzc05hbWUiLCJjb2x1bW5zIiwibWFwIiwibmVzdGVkQmxvayIsIl91aWQiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./components/Grid.js\n");

/***/ }),

/***/ "./components/Page.js":
/*!****************************!*\
  !*** ./components/Page.js ***!
  \****************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _storyblok_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @storyblok/react */ \"@storyblok/react\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_storyblok_react__WEBPACK_IMPORTED_MODULE_1__]);\n_storyblok_react__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\nconst Page = ({ blok  })=>/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"main\", {\n        ...(0,_storyblok_react__WEBPACK_IMPORTED_MODULE_1__.storyblokEditable)(blok),\n        children: blok.body.map((nestedBlok)=>/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_storyblok_react__WEBPACK_IMPORTED_MODULE_1__.StoryblokComponent, {\n                blok: nestedBlok\n            }, nestedBlok._uid, false, {\n                fileName: \"/home/toobler/Desktop/nextjs/components/Page.js\",\n                lineNumber: 6,\n                columnNumber: 7\n            }, undefined))\n    }, void 0, false, {\n        fileName: \"/home/toobler/Desktop/nextjs/components/Page.js\",\n        lineNumber: 4,\n        columnNumber: 3\n    }, undefined);\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Page);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jb21wb25lbnRzL1BhZ2UuanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBO0FBQXlFO0FBRXpFLE1BQU1FLE9BQU8sQ0FBQyxFQUFFQyxLQUFJLEVBQUUsaUJBQ3BCLDhEQUFDQztRQUFNLEdBQUdKLG1FQUFpQkEsQ0FBQ0csS0FBSztrQkFDOUJBLEtBQUtFLElBQUksQ0FBQ0MsR0FBRyxDQUFDLENBQUNDLDJCQUNkLDhEQUFDTixnRUFBa0JBO2dCQUFDRSxNQUFNSTtlQUFpQkEsV0FBV0MsSUFBSTs7Ozs7Ozs7OztBQUtoRSxpRUFBZU4sSUFBSUEsRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovL2Jsb2cvLi9jb21wb25lbnRzL1BhZ2UuanM/NzZlZiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBzdG9yeWJsb2tFZGl0YWJsZSwgU3RvcnlibG9rQ29tcG9uZW50IH0gZnJvbSBcIkBzdG9yeWJsb2svcmVhY3RcIjtcbiBcbmNvbnN0IFBhZ2UgPSAoeyBibG9rIH0pID0+IChcbiAgPG1haW4gey4uLnN0b3J5Ymxva0VkaXRhYmxlKGJsb2spfT5cbiAgICB7Ymxvay5ib2R5Lm1hcCgobmVzdGVkQmxvaykgPT4gKFxuICAgICAgPFN0b3J5Ymxva0NvbXBvbmVudCBibG9rPXtuZXN0ZWRCbG9rfSBrZXk9e25lc3RlZEJsb2suX3VpZH0gLz5cbiAgICApKX1cbiAgPC9tYWluPlxuKTtcbiBcbmV4cG9ydCBkZWZhdWx0IFBhZ2U7Il0sIm5hbWVzIjpbInN0b3J5Ymxva0VkaXRhYmxlIiwiU3RvcnlibG9rQ29tcG9uZW50IiwiUGFnZSIsImJsb2siLCJtYWluIiwiYm9keSIsIm1hcCIsIm5lc3RlZEJsb2siLCJfdWlkIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./components/Page.js\n");

/***/ }),

/***/ "./components/Teaser.js":
/*!******************************!*\
  !*** ./components/Teaser.js ***!
  \******************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _storyblok_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @storyblok/react */ \"@storyblok/react\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_storyblok_react__WEBPACK_IMPORTED_MODULE_1__]);\n_storyblok_react__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\nconst Teaser = ({ blok  })=>{\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h2\", {\n        ...(0,_storyblok_react__WEBPACK_IMPORTED_MODULE_1__.storyblokEditable)(blok),\n        children: blok.headline\n    }, void 0, false, {\n        fileName: \"/home/toobler/Desktop/nextjs/components/Teaser.js\",\n        lineNumber: 4,\n        columnNumber: 10\n    }, undefined);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Teaser);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jb21wb25lbnRzL1RlYXNlci5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFBcUQ7QUFFckQsTUFBTUMsU0FBUyxDQUFDLEVBQUVDLEtBQUksRUFBRSxHQUFLO0lBQzNCLHFCQUFPLDhEQUFDQztRQUFJLEdBQUdILG1FQUFpQkEsQ0FBQ0UsS0FBSztrQkFBR0EsS0FBS0UsUUFBUTs7Ozs7O0FBQ3hEO0FBRUEsaUVBQWVILE1BQU1BLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9ibG9nLy4vY29tcG9uZW50cy9UZWFzZXIuanM/ZjJiMSJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBzdG9yeWJsb2tFZGl0YWJsZSB9IGZyb20gXCJAc3RvcnlibG9rL3JlYWN0XCI7XG4gXG5jb25zdCBUZWFzZXIgPSAoeyBibG9rIH0pID0+IHtcbiAgcmV0dXJuIDxoMiB7Li4uc3RvcnlibG9rRWRpdGFibGUoYmxvayl9PntibG9rLmhlYWRsaW5lfTwvaDI+O1xufTtcbiBcbmV4cG9ydCBkZWZhdWx0IFRlYXNlcjsiXSwibmFtZXMiOlsic3RvcnlibG9rRWRpdGFibGUiLCJUZWFzZXIiLCJibG9rIiwiaDIiLCJoZWFkbGluZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./components/Teaser.js\n");

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../styles/globals.css */ \"./styles/globals.css\");\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_styles_globals_css__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _storyblok_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @storyblok/react */ \"@storyblok/react\");\n/* harmony import */ var _components_Feature__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Feature */ \"./components/Feature.js\");\n/* harmony import */ var _components_Grid__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/Grid */ \"./components/Grid.js\");\n/* harmony import */ var _components_Page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/Page */ \"./components/Page.js\");\n/* harmony import */ var _components_Teaser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/Teaser */ \"./components/Teaser.js\");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_storyblok_react__WEBPACK_IMPORTED_MODULE_2__, _components_Feature__WEBPACK_IMPORTED_MODULE_3__, _components_Grid__WEBPACK_IMPORTED_MODULE_4__, _components_Page__WEBPACK_IMPORTED_MODULE_5__, _components_Teaser__WEBPACK_IMPORTED_MODULE_6__]);\n([_storyblok_react__WEBPACK_IMPORTED_MODULE_2__, _components_Feature__WEBPACK_IMPORTED_MODULE_3__, _components_Grid__WEBPACK_IMPORTED_MODULE_4__, _components_Page__WEBPACK_IMPORTED_MODULE_5__, _components_Teaser__WEBPACK_IMPORTED_MODULE_6__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);\n\n\n\n\n\n\n\nconst components = {\n    feature: _components_Feature__WEBPACK_IMPORTED_MODULE_3__[\"default\"],\n    grid: _components_Grid__WEBPACK_IMPORTED_MODULE_4__[\"default\"],\n    teaser: _components_Teaser__WEBPACK_IMPORTED_MODULE_6__[\"default\"],\n    page: _components_Page__WEBPACK_IMPORTED_MODULE_5__[\"default\"]\n};\n(0,_storyblok_react__WEBPACK_IMPORTED_MODULE_2__.storyblokInit)({\n    accessToken: \"JP24rdmxYChfAYLEX2DtQgtt\",\n    use: [\n        _storyblok_react__WEBPACK_IMPORTED_MODULE_2__.apiPlugin\n    ],\n    components\n});\nfunction MyApp({ Component , pageProps  }) {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n        ...pageProps\n    }, void 0, false, {\n        fileName: \"/home/toobler/Desktop/nextjs/pages/_app.js\",\n        lineNumber: 24,\n        columnNumber: 10\n    }, this);\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyApp);\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUE4QjtBQUM4QjtBQUVoQjtBQUNOO0FBQ0E7QUFDSTtBQUUxQyxNQUFNTSxhQUFhO0lBQ2pCQyxTQUFTTCwyREFBT0E7SUFDaEJNLE1BQU1MLHdEQUFJQTtJQUNWTSxRQUFRSiwwREFBTUE7SUFDZEssTUFBTU4sd0RBQUlBO0FBQ1o7QUFHQUosK0RBQWFBLENBQUM7SUFDWlcsYUFBYTtJQUNiQyxLQUFLO1FBQUNYLHVEQUFTQTtLQUFDO0lBQ2hCSztBQUNGO0FBRUEsU0FBU08sTUFBTSxFQUFFQyxVQUFTLEVBQUVDLFVBQVMsRUFBRSxFQUFFO0lBQ3ZDLHFCQUFPLDhEQUFDRDtRQUFXLEdBQUdDLFNBQVM7Ozs7OztBQUNqQztBQUVBLGlFQUFlRixLQUFLQSxFQUFBIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vYmxvZy8uL3BhZ2VzL19hcHAuanM/ZTBhZCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgJy4uL3N0eWxlcy9nbG9iYWxzLmNzcydcbmltcG9ydCB7IHN0b3J5Ymxva0luaXQsIGFwaVBsdWdpbiB9IGZyb20gXCJAc3RvcnlibG9rL3JlYWN0XCI7XG5cbmltcG9ydCBGZWF0dXJlIGZyb20gXCIuLi9jb21wb25lbnRzL0ZlYXR1cmVcIjtcbmltcG9ydCBHcmlkIGZyb20gXCIuLi9jb21wb25lbnRzL0dyaWRcIjtcbmltcG9ydCBQYWdlIGZyb20gXCIuLi9jb21wb25lbnRzL1BhZ2VcIjtcbmltcG9ydCBUZWFzZXIgZnJvbSBcIi4uL2NvbXBvbmVudHMvVGVhc2VyXCI7XG5cbmNvbnN0IGNvbXBvbmVudHMgPSB7XG4gIGZlYXR1cmU6IEZlYXR1cmUsXG4gIGdyaWQ6IEdyaWQsXG4gIHRlYXNlcjogVGVhc2VyLFxuICBwYWdlOiBQYWdlLFxufTtcblxuXG5zdG9yeWJsb2tJbml0KHtcbiAgYWNjZXNzVG9rZW46IFwiSlAyNHJkbXhZQ2hmQVlMRVgyRHRRZ3R0XCIsXG4gIHVzZTogW2FwaVBsdWdpbl0sXG4gIGNvbXBvbmVudHMsXG59KTtcblxuZnVuY3Rpb24gTXlBcHAoeyBDb21wb25lbnQsIHBhZ2VQcm9wcyB9KSB7XG4gIHJldHVybiA8Q29tcG9uZW50IHsuLi5wYWdlUHJvcHN9IC8+XG59XG5cbmV4cG9ydCBkZWZhdWx0IE15QXBwXG4iXSwibmFtZXMiOlsic3RvcnlibG9rSW5pdCIsImFwaVBsdWdpbiIsIkZlYXR1cmUiLCJHcmlkIiwiUGFnZSIsIlRlYXNlciIsImNvbXBvbmVudHMiLCJmZWF0dXJlIiwiZ3JpZCIsInRlYXNlciIsInBhZ2UiLCJhY2Nlc3NUb2tlbiIsInVzZSIsIk15QXBwIiwiQ29tcG9uZW50IiwicGFnZVByb3BzIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/_app.js\n");

/***/ }),

/***/ "./styles/globals.css":
/*!****************************!*\
  !*** ./styles/globals.css ***!
  \****************************/
/***/ (() => {



/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "@storyblok/react":
/*!***********************************!*\
  !*** external "@storyblok/react" ***!
  \***********************************/
/***/ ((module) => {

"use strict";
module.exports = import("@storyblok/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.js"));
module.exports = __webpack_exports__;

})();