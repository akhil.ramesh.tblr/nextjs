import Head from "next/head";
import Link from "next/link";
import styles from "../../styles/Home.module.css";

export default function Blogs_ssr({blogList}){
    return(
        <>
            <Head>
                <title>Blogs</title>
            </Head>
            <main className={styles.main}>
          <div className={styles.grid}>
            {blogList&& blogList.length
              ? blogList.map((blog) => (
                
                  <Link
                    href={`/blogs_ssr/${blog.slug}`}
                    className={styles.card}
                    key={blog.slug}
                  >
                    <h2>{blog.content.Title}</h2>
                  </Link>
                ))
              : null}
          </div>
        </main>
        </>
    )
}