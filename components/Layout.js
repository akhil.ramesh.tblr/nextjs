import Header from "./Header";
import styles from "../styles/Home.module.css";


export default function Layout({headContent,children}){
    
    return(
        <div className={styles.container}>
            <main className={styles.main}>
                <Header headContent={headContent}/>
                {children}
            </main>
        </div>
    )
}