
import Link from "next/link";
import styles from "../../styles/Home.module.css";

export default function HomePage({homeData,homeDatas}){
    return(
        <>

    <div className={styles.container}>
      <main className={styles.main}>
        <h1 className={styles.title}>
          {homeData.Title}
        </h1>

        <p className={styles.description}>
          <Link href="/blogs" style={{"color": "blue"}}> 
            {homeDatas.Title}
          </Link>
        </p>
      </main>
    </div>
        </>
    )
}