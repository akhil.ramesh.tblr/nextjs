import Link from "next/link";

export default function Header({headContent}){
    console.log(headContent,"content")
    return(
        
        <ul>
             {headContent?.head && headContent?.head.length
        ? headContent?.head.map((index) => (
            
            <li style={{ "display": "inline", "padding" : "10px" }} >
              <Link href={"/"}>{index.label}</Link>
            </li>
          ))
        : null}
        </ul>
    )

}