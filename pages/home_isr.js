import {getHomeItems,getHeader} from '../lib/api'
import Layout from '../components/Layout';
import HomePage from '../components/home';


export async function getStaticProps(){
  const data= await (getHomeItems())||null;
  const headData= await (getHeader())||null;

  return{
    props:{
      headData,
      data,
    },
    revalidate:10,
  };
}

export default function Home({data=null,headData=null}) {
  const homeData=data && data.length>0 && data[1];
  const homeDatas=data && data.length>0 && data[2];
  const headContent= headData && headData.length>0 && headData[0].content;
  return (
    <Layout headContent={headContent}>
        <HomePage homeData={homeData} homeDatas={homeDatas}/>
    </Layout>
  )
}
