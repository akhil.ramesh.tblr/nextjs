
import { getBlogs, getHeader} from "../../lib/api";
import Blogs_ssr from "../../components/blog/blogList_ssr";
import Layout from "../../components/Layout";

export async function getServerSideProps() {
  
    const headData= (await getHeader())||null;
    const blogList = (await getBlogs())|| null;
   
    return {
      props: {
        headData,
        blogList,
      },
      // revalidate: 10,
    };
   
  }
  
  export default function BlogList({blogList=null,headData=null}) {
    console.log(blogList,"blogsss")
    console.log(headData[0],"head_data")
    const headContent= headData && headData.length>0 && headData[0].content;
  
    return (
      <Layout headContent={headContent}>
        <Blogs_ssr blogList={blogList}/>
      </Layout>
   
    );
  }
  
  