import {getBlogsPath,getSingleBlog,getHeader} from '../../lib/api'
import Layout from '../../components/Layout';
import SingleBlog from '../../components/blog/singleBlog';


export async function getStaticProps({params}){
  const headData= (await getHeader())||null;
  const data= await getSingleBlog(params.slug)|| null;
        return{
            props:{
                headData,
                data,
            },
            revalidate:10,
        };
}

export async function getStaticPaths() {
    const paths = await getBlogsPath();
    return {
      paths,
      fallback: false,
    };
  }

export default function Blog({data=null,headData=null}){

  const headContent= headData && headData.length>0 && headData[0].content;
    return(
        <Layout headContent={headContent}>
          <SingleBlog data={data}/>
        </Layout>
    )
}