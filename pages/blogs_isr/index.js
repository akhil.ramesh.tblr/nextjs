
import { getBlogs, getHeader} from "../../lib/api";
import Layout from "../../components/Layout";
import Blogs_isr from "../../components/blog/blogList_isr";

export async function getStaticProps() {
  
    const headData= (await getHeader())||null;
    const blogList = (await getBlogs())|| null;
   
    return {
      props: {
        headData,
        blogList,
      },
      revalidate: 10,
    };
   
  }
  

  export default function BlogList({blogList=null,headData=null}) {
    console.log(blogList,"blogsss")
    console.log(headData[0],"head_data")
    const headContent= headData && headData.length>0 && headData[0].content;
    console.log(headContent,"kittooooo")
    return (
      <Layout headContent={headContent}>
        <Blogs_isr blogList={blogList}/>
      </Layout>
   
    );
  }
  
  