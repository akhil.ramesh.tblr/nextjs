async function fetchAPI(query, { variables, preview } = {}) {
    const res = await fetch("https://gapi.storyblok.com/v1/api", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Token: "JP24rdmxYChfAYLEX2DtQgtt",
        Version: preview ? "draft" : "published",
      },
      body: JSON.stringify({
        query,
        variables,
      }),
    });
  
    const json = await res.json();
    if (json.errors) {
      console.error(json.errors);
      throw new Error("Failed to fetch API");
    }
  
    return json.data;
  }
  export async function getHomeItems(){
    const data= await fetchAPI(`
    {
      PageItem(id: "/home") {
        content {
          body
        }
      }
    }
    
    
    `);
    
    return data?.PageItem?.content?.body;
    
    
  }

  export async function getBlogs(){
    const data= await fetchAPI(`
    {
      BlogItems {
        items {
          content {
            Body
            Title
          }
          slug
        }
      }
    }
    
    `);
    
    return data?.BlogItems?.items;
    
}

export async function getBlogsPath(){
  const data= await fetchAPI(`
  
  {
    BlogItems {
      items {
        slug
      }
    }
  }
  `);
  return data?.BlogItems?.items.map((item) => {
    return { params: { slug: item.slug } };
  });

}

export async function getSingleBlog(slug){
  const data= await fetchAPI(`
  query singleBlog($slug:ID!)
  {
    BlogItem(id: $slug) {
      content {
        Body
        Title
      }
    }
  }
    ` ,{
    preview: true,
    variables: {
      slug: `blogs/${slug}`,
    },
  }
  );
  return data?.BlogItem?.content;
  
}

export async function getHeader(){
  const head= await fetchAPI(`
  {
    HeaderItems {
      items {
        content {
          head
        }
      }
    }
  }
  
  `);
  console.log(head,"========--")
  return head?.HeaderItems?.items;
 
}
